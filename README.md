# Toolbox

Set of handy tools for various of things

## pwned.sh

Has your password been pwned?  
Script calculates SHA1 hash of input string  
Takes 5 letters from the beggining  
Calls the website which retruns all passwords staring with these letters  
https://api.pwnedpasswords.com/range/hash  
Compares returned hashes with generated hash  
If hash is contained in the list, it prints how many times it has been hacked  
Inspired by [Have You Been Pwned](https://www.youtube.com/watch?v=hhUb5iknVJs) by Dr Pound from Computerphile  
