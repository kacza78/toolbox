// Dry Suit Dryer is a tool to help getting the dry suit dry :) 
// Requires an Arduino (Nano v3) + set of two air pumps connected
// via transistors to digital pins (RED and GREEN)
//
// In order to make it work with unofficial board select 
// Tools->Processor->ATMega328P (Old bootloader)

#define GREEN_WIRE 3 // D3
#define RED_WIRE 4 // D4

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(GREEN_WIRE, OUTPUT);
  pinMode(RED_WIRE, OUTPUT);

  Serial.begin(9600);
}


void loop() {
  Serial.println("Beginning of the loop...");
  digitalWrite(RED_WIRE, LOW);
  digitalWrite(GREEN_WIRE, LOW);

  Serial.print("Enabling Green wire on pin ");
  Serial.println(GREEN_WIRE);
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(GREEN_WIRE, HIGH);
  delay(5000);                       // wait for a second
  Serial.println("Disabling Green wire.");
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(GREEN_WIRE, LOW);
  delay(1000);                       // wait for a second
  Serial.print("Enabling Red wire on pin ");
  Serial.println(RED_WIRE);
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(RED_WIRE, HIGH);
  delay(5000);                       // wait for a second
  Serial.println("Disabling Red wire.");
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(GREEN_WIRE, LOW);
  digitalWrite(RED_WIRE, LOW);
  delay(1000);                       // wait for a second
  Serial.println("End of the loop...");
}
