#include <cstdint>


template <typename T, std::size_t S>
inline constexpr std::size_t get_file_name_offset(const T (& str)[S], size_t i = S - 1)
{
    return (str[i] == '/') ?
            (i + 1) :
            (i > 0 ?
                get_file_name_offset(str, i - 1) :
                0
            );
}

template <typename T>
inline constexpr std::size_t get_file_name_offset(T (& str)[1])
{
    return 0;
}


namespace utility {

    template <typename T, T v>
    struct const_expr_value
    {
        static constexpr const T value = v;
    };

}
#define UTILITY_CONST_EXPR_VALUE(exp) ::utility::const_expr_value<decltype(exp), exp>::value

#define LEAF(FN) (&FN[UTILITY_CONST_EXPR_VALUE(get_file_name_offset(FN))])
