#include <iostream>

#include "leaf.hpp"

#define FILE  (__builtin_strrchr (__FILE__, '/') ? __builtin_strrchr (__FILE__, '/') + 1 : __FILE__)

int main()
{
    std::cout << __FILE__ << std::endl;
    std::cout << LEAF(__FILE__) << std::endl;
    std::cout << FILE << std::endl;
    std::cout << __PRETTY_FUNCTION__ << std::endl;

}
