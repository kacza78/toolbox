#!/bin/bash

if [ -z $1 ]; then
    echo "Error: Argument missing!"
    echo "Usage:"
    echo -e "\t$0 password"
    exit 1
fi

echo "Generating a hash of your password..."
pwd_sha1=$(echo -n $1 | sha1sum)    # calculate SHA1 of an argument
pwd_sha1=${pwd_sha1:0:40}           # cut 40 chars from left
pwd_sha1=${pwd_sha1^^}              # to upper

pwd_sha1_key=${pwd_sha1:0:5}        # take 5 chars from left
echo "Hash: $pwd_sha1"

# Concatenate the URL
url=https://api.pwnedpasswords.com/range/${pwd_sha1_key}

echo "Getting list of matching passwords for $pwd_sha1_key..."
lines=$(curl -s $url)
matches_count=$(echo "$lines" | wc -l)
echo "Found $matches_count matching passwords"

echo "Searching for password's hash match..."
for line in $lines
do
    hash=`echo $line | cut -d : -f 1 | tr -d [:cntrl:][:blank:]`        # cut hash from line
    hash="$pwd_sha1_key$hash"                                           # add key
    if [ "$pwd_sha1" == "$hash" ]; then
        count=`echo $line | cut -d : -f 2 | tr -d [:cntrl:][:blank:]`   # cut count from line

        if [ "$count" -eq 0 ]; then
            echo "Hash match found but without hacks"
            exit 0
        fi

        echo "Warning! Match found, hacks count: $count"
        exit 0
    fi
done

echo Match not found! You are safe


