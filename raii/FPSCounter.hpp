#pragma once

#include <chrono>
#include <iostream>

class FPSCounter final
{
public:
    void tick()
    {
        if (count++ >= 50)
        {
            const auto current = std::chrono::steady_clock::now();
            const auto elapsed_msec = std::chrono::duration_cast<std::chrono::milliseconds>(current - start);

            const float fps = (1000.0f * count) / static_cast<float>(elapsed_msec.count());
            std::cout << "Current FPS: " << fps << ", time: " << elapsed_msec.count() << "ms" << std::endl;
            start = current;
            count = 0;
        }
    }

private:
    std::size_t count = 0;
    std::chrono::time_point<std::chrono::steady_clock> start = std::chrono::steady_clock::now();

};