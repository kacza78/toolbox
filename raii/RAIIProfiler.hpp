#ifndef RAII_PROFILER_HPP
#define RAII_PROFILER_HPP

#include <chrono>
#include <string>

template <class T = void>
struct StaticCounter
{
    static unsigned int indent;
};

template <class T>
unsigned int StaticCounter<T>::indent;

struct RAIIProfiler : private StaticCounter<>
{
    RAIIProfiler(const std::string &name = ""): name(name)
    {
        start = std::chrono::high_resolution_clock::now();
        ++indent;
    }

    ~RAIIProfiler()
    {
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> ms = end - start;

        for (std::size_t i = 0; i < indent - 1; ++i)
        {
            printf(" ");
        }

        printf("|- raiiprof - %s time spent: \t%.2lfms\n", name.c_str(), ms.count());
        --indent;
    }

    RAIIProfiler(RAIIProfiler &) = delete;
    RAIIProfiler(RAIIProfiler &&) = delete;

private:
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
    std::string name;
};

#endif // RAII_PROFILER_HPP
