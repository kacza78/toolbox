#pragma once

#include <chrono>
#include <iostream>
#include <thread>

struct RAIIRatio final
{
    RAIIRatio(const int hz)
    {
        start = std::chrono::steady_clock::now();
        const auto duration = 1000.0f / static_cast<float>(hz);
        max_sleep_len_ms = std::chrono::milliseconds(static_cast<int>(duration));

        std::cout << "RAIIRatio max sleep len=" << max_sleep_len_ms.count() << "ms\n";

    }
    void sleep()
    {
        std::chrono::steady_clock::time_point current = std::chrono::steady_clock::now();
        const auto spent = current - start;
        // const auto msecs = std::chrono::duration_cast<std::chrono::milliseconds>(spent);

        // std::cout << "RAIIRation spent " << msecs.count() << "ms\n";
        const auto remaining = max_sleep_len_ms - spent;
        // const auto remaining_ms = std::chrono::duration_cast<std::chrono::milliseconds>(remaining);
        // std::cout << "RAIIRation remaining " << remaining_ms.count() << "ms\n";

        std::this_thread::sleep_for(remaining);
        start = std::chrono::steady_clock::now();
    }
private:

    std::chrono::steady_clock::time_point start;
    std::chrono::milliseconds max_sleep_len_ms;
};