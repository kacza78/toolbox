#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"

class LCD
{
    const uint RS_PIN = 16;
    const uint EN_PIN = 17;
    const uint D4 = 13;
    const uint D5 = 12;
    const uint D6 = 11;
    const uint D7 = 10;

public:
    LCD(uint RS_PIN, uint EN_PIN,
        uint D4,uint D5, uint D6, uint D7) :
        RS_PIN(RS_PIN),
        EN_PIN(EN_PIN),
        D4(D4), D5(D5), D6(D6), D7(D7)
    {
        sleep_ms(50); // delay required to power up the LCD
        init_pin(RS_PIN, GPIO_OUT);
        init_pin(EN_PIN, GPIO_OUT);
        init_pin(D4, GPIO_OUT);
        init_pin(D5, GPIO_OUT);
        init_pin(D6, GPIO_OUT);
        init_pin(D7, GPIO_OUT);

        printf("Initializing\n");
        gpio_put(RS_PIN, 0); // RW is pulled down
        lcd_cmd(0x28); // 4 bit communication, 2 lines, 5x8 dots

        printf("Return home\n");
        lcd_cmd(0x02); // return home

        printf("Cursor\n");
        lcd_cmd(0x0d); // cursor on, blinking
        lcd_cmd(0x80);

        clear_screen();
    }

    void set_cursor(uint col, uint row)
    {
        printf("set_cursor col=%d, row=%d\n", col, row);
        if (row == 1)
        {
            lcd_cmd(0x80 + 0x40 + col);
        }
        else
        {
            lcd_cmd(0x80 + col);
        }
    }

    void print(const char *str)
    {
        uint len = MIN(16, strlen(str));
        for (uint i = 0; i < len; ++i)
        {
            lcd_char(str[i]);
        }
    }

    void clear_screen()
    {
        printf("Clear\n");
        lcd_cmd(0x01); // clear
    }

private:
    void init_pin(uint pin, uint dir)
    {
        gpio_init(pin);
        gpio_set_dir(pin, dir);
    }

    void lcd_cmd (char cmd)
    {
        // RS=0;RW=0;
        gpio_put(RS_PIN, 0); // RW is pulled down

        set_pins(cmd);
    }

    void lcd_char(char c)
    {
        // RS=1;RW=0;
        gpio_put(RS_PIN, 1); // RW is pulled down

        set_pins(c);
    }


    void set_pins(char c)
    {
        printf("pins: %x (%c)\n", c, c);
        gpio_put(D4, (c >> 4) & 0x01);
        gpio_put(D5, (c >> 5) & 0x01);
        gpio_put(D6, (c >> 6) & 0x01);
        gpio_put(D7, (c >> 7) & 0x01);
        enable();

        gpio_put(D4, (c >> 0) & 0x01);
        gpio_put(D5, (c >> 1) & 0x01);
        gpio_put(D6, (c >> 2) & 0x01);
        gpio_put(D7, (c >> 3) & 0x01);
        enable();
    }

    void enable()
    {
        gpio_put(EN_PIN, 0);
        sleep_us(100);
        gpio_put(EN_PIN, 1);
        sleep_us(100);
        gpio_put(EN_PIN, 0);
        sleep_us(100);
    }
};

int main()
{
    const uint LED_PIN = 25;

    const uint RS_PIN = 16;
    const uint EN_PIN = 17;

    const uint D4 = 13;
    const uint D5 = 12;
    const uint D6 = 11;
    const uint D7 = 10;

    stdio_init_all();
    printf("LCD welcomes\n");

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);


    LCD lcd(RS_PIN, EN_PIN, D4, D5, D6, D7);
    lcd.print("Test line 1");
    lcd.set_cursor(0, 1);
    lcd.print("Test line 2");

    gpio_put(LED_PIN, 1);
    sleep_ms(500);
    gpio_put(LED_PIN, 0);
    sleep_ms(500);

    lcd.clear_screen();
    lcd.set_cursor(0, 0);
    lcd.print("Another line 1");
    lcd.set_cursor(4, 1);
    lcd.print("Another line 2");
}
