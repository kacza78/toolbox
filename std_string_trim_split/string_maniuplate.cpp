#include <algorithm>
#include <cctype>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

std::string trim(const std::string &s)
{
    auto wsfront = std::find_if_not(s.begin(), s.end(), [](int c) { return std::isspace(c); });
    return std::string(wsfront, std::find_if_not(s.rbegin(), std::string::const_reverse_iterator(wsfront), [](int c) { return std::isspace(c); }).base());
}

std::string trim2(const std::string &s)
{
    const char whitespace[] = " \t\n";
    const size_t first(s.find_first_not_of(whitespace));
    if (first == std::string::npos)
    {
        return {};
    }
    const size_t last(s.find_last_not_of(whitespace));
    return s.substr(first, (last - first + 1));
}

std::vector<std::string> split(const std::string &s, char delim)
{
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> elems;
    while (std::getline(ss, item, delim))
    {
        item = trim(item);
        if (item.length())
            elems.push_back(std::move(item));
    }
    return elems;
}

std::string getLine(const std::string &s, std::size_t position)
{
    if (position >= s.size() || s[position] == '\n')
    {
        return "";
    }

    std::size_t startPos = s.rfind('\n', position);
    if (startPos == std::string::npos)
    {
        startPos = 0;
    }
    else
    {
        ++startPos; // skip leading new line character
    }

    std::size_t finishPos = s.find('\n', position);
    if (finishPos == std::string::npos)
    {
        finishPos = s.size();
    }

    if (finishPos <= startPos)
    {
        return "";
    }

    return s.substr(startPos, finishPos - startPos);
}

void test()
{
    std::string test("\n\n\n   sample test abc\ndef");
    for (std::size_t i = 0; i < test.size(); ++i)
        std::cout << "result: " << i << " '" << getLine(test, i) << "'\n";
}